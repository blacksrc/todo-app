# Simple Todo App
Simple todo app based on React and Redux.

### Installation
Clone and the use this command to install dependencies.
> Consider that you have to install npm first.

`npm i`

---

### Development
In order to develop the project you can use this command.

`npm start`

---

### Production Build
To build a production version use the command below.

`npm build`