import { addTask, updateTaskState, removeTask, updateTaskText } from "./task/actions";
import { setVisible } from "./filter/actions";

const actions = {
  tasks: {
    addTask,
    updateTaskState,
    removeTask,
    updateTaskText,
  },
  filter: {
    setVisible
  }
}

export default actions;
