import { combineReducers } from "redux";
import taskReducer from "./task/reducers";
import filterReducer from "./filter/reducers";

const rootReducer = combineReducers({
  tasks: taskReducer,
  filter: filterReducer,
});

export default rootReducer;