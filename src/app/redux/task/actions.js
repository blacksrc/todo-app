import { ADD_TASK, UPDATE_TASK_STATE, REMOVE_TASK, UPDATE_TASK_TEXT } from "./types";

export const addTask = task => ({ 
  type: ADD_TASK,
  payload: task
});

export const updateTaskState = task => ({ 
  type: UPDATE_TASK_STATE,
  payload: task
});

export const removeTask = task => ({ 
  type: REMOVE_TASK,
  payload: task
});

export const updateTaskText = (task, newText) => ({ 
  type: UPDATE_TASK_TEXT,
  payload: { task, newText }
});