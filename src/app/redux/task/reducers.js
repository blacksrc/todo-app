import { ADD_TASK, UPDATE_TASK_STATE, REMOVE_TASK, UPDATE_TASK_TEXT } from "./types";

let initialState = [];

const taskReducer = (state = initialState, action) => {

  switch (action.type) {

    case ADD_TASK:
      return [ action.payload, ...state ]

    case UPDATE_TASK_STATE:
      let newState = action.payload.state;
      switch (action.payload.state) {
        case 1:
          newState = 2;
          break;
        case 2:
          newState = 3;
          break;
        case 3:
          newState = 1;
          break;
        default:
          newState = action.payload.state;
          break;
      }

      return state.map((item) => (
        item.id === action.payload.id ? {...item, state: newState}: item
      ));

    case REMOVE_TASK:
      return state.filter(task => task.id !== action.payload.id);

    case UPDATE_TASK_TEXT:
      return state.map((task) => (
        task.id === action.payload.task.id ? {...task, text: action.payload.newText}: task
      ));
      
    default:
      return state;
  }
};

export default taskReducer;