import { SET_VISIBLE } from "./types";

let initialState = 0;

const filterReducer = (state = initialState, action) => {

  switch (action.type) {

    case SET_VISIBLE:
      return action.payload

    default:
      return state;
  }
};

export default filterReducer;