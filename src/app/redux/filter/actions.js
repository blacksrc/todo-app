import { SET_VISIBLE } from "./types";

export const setVisible = filter => ({ 
  type: SET_VISIBLE,
  payload: filter
});