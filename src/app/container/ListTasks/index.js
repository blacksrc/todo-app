import { connect } from "react-redux";
import Tasks from "./Tasks.jsx";

const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    filter: state.filter
  };
};

export default connect(mapStateToProps)(Tasks);
