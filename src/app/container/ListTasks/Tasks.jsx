import React, { Component } from "react";
import TaskItem from './../TaskItem';

class Tasks extends Component {
	render() {
		let { tasks } = this.props;

		if (tasks.length === 0) {
			return <div className="text-info p-2">Add your first task ;)</div>;
		}
		
		return ( 
			<ul className="list-group mt-2">
				{tasks.map((task) => {
					if (task.state !== this.props.filter && this.props.filter !== 0) {
						return  null;
					}
					return <TaskItem key={task.id} task={task} />
				})}
			</ul>
		);
	}
}

export default Tasks;