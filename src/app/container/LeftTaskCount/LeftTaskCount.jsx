import React, { Component } from "react";

class LeftTaskCount extends Component {

  render() {

    const todoTaskLeftCount = this.props.tasks.filter(task => task.state === 1).length;

    return (
      todoTaskLeftCount > 0 ?
        <h3 href="#" className="float-right">
          <span className="badge badge-dark" title="Delete">
            {todoTaskLeftCount} Task{todoTaskLeftCount <= 1 ? '' : 's'} Left
          </span>
        </h3>
      :
      null
    );
  } 
}

export default LeftTaskCount;