import { connect } from "react-redux";
import LeftTaskCount from "./LeftTaskCount.jsx";

const mapStateToProps = state => {
  return {
    tasks: state.tasks
  };
};

export default connect(mapStateToProps)(LeftTaskCount);
