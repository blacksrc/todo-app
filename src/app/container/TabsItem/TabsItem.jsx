import React, { Component } from "react";

class Tabs extends Component {

  constructor(props) {
    super(props);
  }

  setFilterHandler(filter) {
    this.props.setVisible(filter);
  }

  render() {
    let { color, title } = this.props.tab;
    const isChecked = this.props.filter === this.props.state;

    return (
      <label className={`btn btn-light ${isChecked ? 'active' : ''}`} onClick={() => this.setFilterHandler(this.props.state)} role="button">
        <input type="radio" defaultChecked={isChecked} />
        <span style={{color: color}}>{title}</span>
      </label>
    );
  } 
}

export default Tabs;