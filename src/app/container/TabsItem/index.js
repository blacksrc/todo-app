import { connect } from "react-redux";
import TabsItem from "./TabsItem.jsx";
import Actions from "./../../redux/Actions";

const mapStateToProps = state => {
  return {
    filter: state.filter
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setVisible: filter => dispatch(Actions.filter.setVisible(filter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TabsItem);
