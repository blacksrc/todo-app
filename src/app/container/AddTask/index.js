import { connect } from "react-redux";
import AddTask from "./AddTask.jsx";
import Actions from "./../../redux/Actions";

const mapStateToProps = state => {
  return {
    tasks: state.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTask: task => dispatch(Actions.tasks.addTask(task)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);
