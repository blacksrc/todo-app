import React, { Component } from "react";
import uuid from 'uuid/v4';
import moment from 'moment';

class AddTask extends Component {
	constructor() {
		super();

		this.changeHandler = this.changeHandler.bind(this);
		this.submitHandler = this.submitHandler.bind(this);

		this.state = {
      newTask: ""
    };
	}

	changeHandler(e) {
		this.setState({ 
			newTask: e.target.value 
		});
	}
	
	submitHandler(e) {
		e.preventDefault();

		const newTask = this.state.newTask;

		if (newTask.length > 0) {
			this.props.addTask({
				id: uuid(),
				text: newTask, 
				date: moment(), 
				state: 1,
			});
		}

		this.setState({ 
			newTask: ""
		});
	}

	render() {
		return (
			<form id="new-task-form" onSubmit={this.submitHandler}>
				<input
					type="text"
					value={this.state.newTask}
					placeholder="Enter new task..."
					onChange={this.changeHandler}
					className="form-control"
					required
				/>
			</form>	
		);
	}
}

export default AddTask;