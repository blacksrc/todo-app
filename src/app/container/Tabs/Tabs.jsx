import React, { Component } from "react";
import { states } from './../../config/states';
import TabsItem from './../TabsItem';

class Tabs extends Component {

  render() {
    return (
      <div className="btn-group btn-group-toggle" data-toggle="buttons">
        {states.map((state, index) => {
          return <TabsItem key={index} tab={state} state={index} />
        })}
      </div>
    );
  } 
}

export default Tabs;