import { connect } from "react-redux";
import TaskItem from "./TaskItem.jsx";
import Actions from "./../../redux/Actions";

const mapStateToProps = state => {
  return {
    tasks: state.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateTaskState: task => dispatch(Actions.tasks.updateTaskState(task)),
    removeTask: task => dispatch(Actions.tasks.removeTask(task)),
    updateTaskText: (task, newText) => dispatch(Actions.tasks.updateTaskText(task, newText)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskItem);
