import React, { Component } from "react";
import PropTypes from "prop-types";
import ListItemState from "./../../component/ItemState.jsx";
import ListItemDate from "./../../component/ItemDate.jsx";

class TaskItem extends Component {

  constructor(props) {
    super(props);

    this.changeHandler = this.changeHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);

    this.state = {
      editing: false,
      editingTask: null,
      newText: "",
    }
  }

  stateChangeHandler(task) {
    this.props.updateTaskState(task);
  }

  removeTaskHandler(task) {
    this.props.removeTask(task);
  }

  toggleEditHandler(task) {
    this.setState({
      editing: true,
      editingTask: task,
      newText: task.text
    }, () => {
      this.editInput.focus();
    });
  }

  changeHandler(e) {
		this.setState({ 
			newText: e.target.value 
		});
  }
  
  submitHandler(e) {
    e.preventDefault();
    this.setState({
      editing: false,
    }, () => {
      this.props.updateTaskText(this.state.editingTask, this.state.newText);
    });
	}
  
  render() {

    let { text, date, state } = this.props.task;

    return (
      <li className="list-group-item">
        <span onDoubleClick={() => this.toggleEditHandler(this.props.task)}>
          {this.state.editing ? 
          	<form id="new-task-form" onSubmit={this.submitHandler}>
              <input type="text" 
                className="form-control" 
                ref={(input) => { this.editInput = input; }}  
                value={this.state.newText} 
                onChange={this.changeHandler} 
              />
            </form>
            : 
            <React.Fragment>
              {text} <small className="text-muted">(dblclick to edit)</small>
            </React.Fragment>
          }
        </span>
        <a href="#" className="float-left" onClick={() => this.stateChangeHandler(this.props.task)}>
          <ListItemState state={state} />
        </a>
        <br />
        <ListItemDate date={date} />
        <a href="#" className="float-right" onClick={() => this.removeTaskHandler(this.props.task)}>
          <span className="badge badge-danger" title="Delete">x</span>
        </a>
      </li>
    );
  } 
}

TaskItem.propTypes = {
  task: PropTypes.object.isRequired,
};

export default TaskItem;