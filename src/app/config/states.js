export const states = [
  {
    title: "All",
    color: "#333",
  },
  {
    title: "Todo",
    color: "#ffc107",
  },
  {
    title: "Doing",
    color: "#007bff",
  },
  {
    title: "Done",
    color: "#28a745",
  },
]