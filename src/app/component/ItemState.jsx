import React from "react";
import PropTypes from "prop-types";
import { states } from './../config/states';

const ListItemState = ({ state }) => {
	var currentState = states.find((st, index) => index === state);
	return <span className="badge badge-light" style={{color: currentState.color}}>{currentState.title}</span>
};

ListItemState.propTypes = {
  state: PropTypes.number.isRequired,
};

export default ListItemState;