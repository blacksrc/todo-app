import React from "react";
import PropTypes from "prop-types";
import { elapsedDurationFrom } from "../util/util";

const ListItemDate = ({ date }) => {
	return (
		<small className="text-muted"> { elapsedDurationFrom(date) } </small>
	);
};

export default ListItemDate;