import React, { Component } from "react";
import AddTask from "./app/container/AddTask";
import ListTasks from "./app/container/ListTasks";
import Tabs from './app/container/Tabs';
import LeftTaskCount from './app/container/LeftTaskCount';

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row mt-4 mb-4">
					<div className="col-md-6 offset-md-3">
						
            <div className="row">
              <div className="col-9">
                <Tabs />
              </div>
              <div className="col-3">
                <LeftTaskCount />
              </div>
            </div>
						<AddTask />
            <ListTasks />
					
					</div>
        </div>
    	</div>
    );
  }
}

export default App;
